//
//  TemplateNetworkUtils.swift
//  TemplateSDK
//
//  Created by Admin on 14/04/21.
//

import Foundation
import Alamofire


open class TemplateNetworkUtils {
    private static var _defaultHeaders: HTTPHeaders?
    public static var defaultHeaders: HTTPHeaders {
        get { _defaultHeaders ?? [:] }
        set { _defaultHeaders = newValue }
    }
}
