//
//  TemplateUser.swift
//  TemplateSDK
//
//  Created by Admin on 30/03/21.
//

import Foundation
import Alamofire


open class TemplateUser: Codable {
    public var id: Int64?
    public var name: String?
    public var email: String?
    public var timezone: String?
    public var token: String?
    public var profileImage: String?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case email
        case timezone
        case token
        case profileImage = "profile_image"
    }
    
    open class func login<T>(
        type: T.Type,
        url: String, email: String, password: String,
        deviceToken: String? = nil, extraParams: [String: String]? = nil,
        success: ((T) -> Void)?,
        failure: ((_ errorMessage: String?, _ error: Error?) -> Void)?
    ) where T: Codable {
        var params: [String: String] = [
            "email": email,
            "password": password,
            "timezone": TimeZone.current.identifier
        ]
        
        if let deviceToken = deviceToken {
            params["device_token"] = deviceToken
            params["device_type"] = "ios"
        }
        
        if let extraParams = extraParams {
            params.merge(extraParams) { (_, extra) -> String in extra }
        }
        
        AF.request(
            url,
            method: .post,
            parameters: params,
            encoding: URLEncoding.default
        ).validate().response { (response) in
            switch response.result {
                case .success(let data):
                    guard let data = data else {
                        print("Empty data")
                        failure?("Empty data", nil)
                        return
                    }
                    do {
//                        let user = try JSONDecoder().decode(ObjectResponse<TemplateUser>.self, from: data)
                        let user = try JSONDecoder().decode(ObjectResponse<T>.self, from: data)
                        print(user)
                        
                        success?(user.data)
                    } catch let error {
                        print("Parsing error = \n\(error)")
                    }
                    break
                case .failure(let error):
                    print("Failure = \n\(error)")
                    if let data = response.data {
                        print(String(decoding: data, as: UTF8.self))
                        
                        failure?(ErrorHandler.getErrorMessage(data), error)
                    } else {
                        failure?(nil, error)
                    }
                    break
            }
        }
    }
    
    open class func signUp<T>(
        type: T.Type,
        url: String, name: String, email: String, password: String,
        deviceToken: String? = nil, extraParams: [String: String]? = nil,
        success: ((T) -> Void)?,
        failure: ((_ errorMessage: String?, _ error: Error?) -> Void)?
    ) where T: Codable {
        var params: [String: String] = [
            "email": email,
            "password": password,
            "name": name,
            "timezone": TimeZone.current.identifier
        ]
        
        if let deviceToken = deviceToken {
            params["device_token"] = deviceToken
            params["device_type"] = "ios"
        }
        
        if let extraParams = extraParams {
            params.merge(extraParams) { (_, extra) -> String in extra }
        }
        
        AF.request(
            url,
            method: .post,
            parameters: params,
            encoding: URLEncoding.default
        ).validate().response { (response) in
            switch response.result {
                case .success(let data):
                    guard let data = data else {
                        print("Empty data")
                        failure?("Empty data", nil)
                        return
                    }
                    do {
                        let user = try JSONDecoder().decode(ObjectResponse<T>.self, from: data)
                        print(user)
                        
                        success?(user.data)
                    } catch let error {
                        print("Parsing error = \n\(error)")
                    }
                    break
                case .failure(let error):
                    print("Failure = \n\(error)")
                    if let data = response.data {
                        print(String(decoding: data, as: UTF8.self))
                        
                        failure?(ErrorHandler.getErrorMessage(data), error)
                    } else {
                        failure?(nil, error)
                    }
                    break
            }
        }
    }
    
    open class func recovery<T>(
        type: T.Type,
        url: String, email: String,
        extraParams: [String: String]? = nil,
        success: ((T) -> Void)?,
        failure: ((_ errorMessage: String?, _ error: Error?) -> Void)?
    ) where T: Codable {
        var params: [String: String] = [
            "email": email,
        ]
        
        if let extraParams = extraParams {
            params.merge(extraParams) { (_, extra) -> String in extra }
        }
        
        AF.request(
            url,
            method: .post,
            parameters: params,
            encoding: URLEncoding.default
        ).validate().response { (response) in
            switch response.result {
                case .success(let data):
                    guard let data = data else {
                        print("Empty data")
                        failure?("Empty data", nil)
                        return
                    }
                    do {
                        let response = try JSONDecoder().decode(T.self, from: data)
                        print(response)
                        
                        success?(response)
                    } catch let error {
                        print("Parsing error = \n\(error)")
                    }
                    break
                case .failure(let error):
                    print("Failure = \n\(error)")
                    if let data = response.data {
                        print(String(decoding: data, as: UTF8.self))
                        
                        failure?(ErrorHandler.getErrorMessage(data), error)
                    } else {
                        failure?(nil, error)
                    }
                    break
            }
        }
    }
    
    open class func getProfile<T: Codable>(
        type: T.Type,
        url: String,
        extraParams: [String: String]? = nil,
        success: ((T) -> Void)?,
        failure: ((_ errorMessage: String?, _ error: Error?) -> Void)?
    ) {
        var params: [String: String] = [:]
        
        if let extraParams = extraParams {
            params.merge(extraParams) { (_, extra) -> String in extra }
        }
        
        print(url)
        print(TemplateNetworkUtils.defaultHeaders)
        
        AF.request(
            url,
            method: .get,
            parameters: params,
            encoding: URLEncoding.default,
            headers: TemplateNetworkUtils.defaultHeaders
        ).validate().response { (response) in
            switch response.result {
                case .success(let data):
                    guard let data = data else {
                        print("Empty data")
                        failure?("Empty data", nil)
                        return
                    }
                    do {
                        let user = try JSONDecoder().decode(ObjectResponse<T>.self, from: data)
                        print(user)
                        
                        success?(user.data)
                    } catch let error {
                        print("Parsing error = \n\(error)")
                    }
                    break
                case .failure(let error):
                    print("Failure = \n\(error)")
                    if let data = response.data {
                        print(String(decoding: data, as: UTF8.self))
                        
                        failure?(ErrorHandler.getErrorMessage(data), error)
                    } else {
                        failure?(nil, error)
                    }
                    break
            }
        }
    }
    
    open class func updateProfile(
        url: String, name: String? = nil, password: String? = nil, email: String? = nil,
        extraParams: [String: String]? = nil,
        success: (() -> Void)?,
        failure: ((_ errorMessage: String?, _ error: Error?) -> Void)?
    ) {
        var params: [String: String] = [
            "timezone": TimeZone.current.identifier
        ]
        
        if let email = email {
            params["email"] = email
        }
        
        if let name = name {
            params["name"] = name
        }
        
        if let password = password {
            params["password"] = password
        }
        
        if let extraParams = extraParams {
            params.merge(extraParams) { (_, extra) -> String in extra }
        }
        
        AF.request(
            url,
            method: .post,
            parameters: params,
            encoding: URLEncoding.default,
            headers: TemplateNetworkUtils.defaultHeaders
        ).validate().response { (response) in
            switch response.result {
                case .success:
                    success?()
                    break
                case .failure(let error):
                    print("Failure = \n\(error)")
                    if let data = response.data {
                        print(String(decoding: data, as: UTF8.self))
                        
                        failure?(ErrorHandler.getErrorMessage(data), error)
                    } else {
                        failure?(nil, error)
                    }
                    break
            }
        }
    }
    
    open class func updateDeviceToken(
        url: String, deviceToken: String,
        extraParams: [String: String]? = nil,
        success: (() -> Void)?,
        failure: ((_ errorMessage: String?, _ error: Error?) -> Void)?
    ) {
        var params: [String: String] = [
            "device_type": "ios",
            "device_token": deviceToken
        ]
        
        if let extraParams = extraParams {
            params.merge(extraParams) { (_, extra) -> String in extra }
        }
        
        AF.request(
            url,
            method: .post,
            parameters: params,
            encoding: URLEncoding.default,
            headers: TemplateNetworkUtils.defaultHeaders
        ).validate().response { (response) in
            switch response.result {
                case .success:
                    success?()
                    break
                case .failure(let error):
                    print("Failure = \n\(error)")
                    if let data = response.data {
                        print(String(decoding: data, as: UTF8.self))
                        
                        failure?(ErrorHandler.getErrorMessage(data), error)
                    } else {
                        failure?(nil, error)
                    }
                    break
            }
        }
    }
}
