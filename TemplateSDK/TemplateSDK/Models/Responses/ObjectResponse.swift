//
//  ObjectResponse.swift
//  TemplateSDK
//
//  Created by Admin on 30/03/21.
//

import Foundation

public struct ObjectResponse<T: Decodable>: Decodable {
    public var data: T
    
    private enum CodingKeys: String, CodingKey {
        case data
    }
}
