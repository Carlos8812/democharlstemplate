//
//  DefaultRules.swift
//  TemplateSDK
//
//  Created by Admin on 06/04/21.
//

import Foundation

public struct NonEmptyRule: BaseRule {
    var errorMsg: String = "Can't be empty"
    var shouldTrim: Bool = true
    
    public func validate(text: String) -> Bool {
        var _text = text
        if shouldTrim {
            _text = text.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        
        return !_text.isEmpty
    }
    
    public func getErrorMessage() -> String {
        return errorMsg
    }
    
    public mutating func setError(msg: String) {
        self.errorMsg = msg
    }
}

public struct AllLowerCaseRule: BaseRule {
    var errorMsg: String = "All letters should be in lower case"
    var shouldTrim: Bool = false
    
    public func validate(text: String) -> Bool {
        var _text = text
        if shouldTrim {
            _text = text.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        
        return _text == _text.lowercased()
    }
    
    public func getErrorMessage() -> String {
        return errorMsg
    }
    
    public mutating func setError(msg: String) {
        self.errorMsg = msg
    }
}

public struct AllUpperCaseRule: BaseRule {
    var errorMsg: String = "All letters should be in upper case"
    var shouldTrim: Bool = false
    
    public func validate(text: String) -> Bool {
        var _text = text
        if shouldTrim {
            _text = text.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        
        return _text == _text.uppercased()
    }
    
    public func getErrorMessage() -> String {
        return errorMsg
    }
    
    public mutating func setError(msg: String) {
        self.errorMsg = msg
    }
}

public struct ContainsRule: BaseRule {
    var _errorMsg: String? = nil
    var target: String
    var errorMsg: String {
        get { _errorMsg ?? "Should contain \(target)"}
        set { _errorMsg = newValue }
    }
    var shouldTrim: Bool = false
    var ignoreCase: Bool = false
    
    public func validate(text: String) -> Bool {
        var _text = text
        var _target = target
        
        if shouldTrim {
            _text = text.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        
        if ignoreCase {
            _text = _text.lowercased()
            _target = _target.lowercased()
        }
        
        return _text.contains(_target)
    }
    
    public func getErrorMessage() -> String {
        return errorMsg
    }
    
    public mutating func setError(msg: String) {
        self.errorMsg = msg
    }
}

public struct RegexRule: BaseRule {
    var pattern: String
    var errorMsg: String = "RegEx pattern doesn't match"
    var shouldTrim: Bool = false
    
    public func validate(text: String) -> Bool {
        var _text = text
        if shouldTrim {
            _text = text.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        
        let regex = NSPredicate(format: "SELF MATCHES[c] %@", pattern)
        return regex.evaluate(with: _text)
    }
    
    public func getErrorMessage() -> String {
        return errorMsg
    }
    
    public mutating func setError(msg: String) {
        self.errorMsg = msg
    }
}

public struct EmailRule: BaseRule {
    var errorMsg: String = "Invalid Email Address"
    var shouldTrim: Bool = false
    
    public func validate(text: String) -> Bool {
        return RegexRule(
//            pattern: "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{1,4}$",
            pattern: "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9]))|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$",
            errorMsg: errorMsg,
            shouldTrim: shouldTrim
        ).validate(text: text)
    }
    
    public func getErrorMessage() -> String {
        return errorMsg
    }
    
    public mutating func setError(msg: String) {
        self.errorMsg = msg
    }
}

public struct MinLengthRule: BaseRule {
    var _errorMsg: String? = nil
    var errorMsg: String {
        get { _errorMsg ?? "Minimum \(minLength) characters"}
        set { _errorMsg = newValue }
    }
    var minLength: Int
    var shouldTrim: Bool = false
    
    public func validate(text: String) -> Bool {
        var _text = text
        if shouldTrim {
            _text = text.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        
        return _text.count >= minLength
    }
    
    public func getErrorMessage() -> String {
        return errorMsg
    }
    
    public mutating func setError(msg: String) {
        self.errorMsg = msg
    }
}

public struct MaxLengthRule: BaseRule {
    var _errorMsg: String? = nil
    var maxLength: Int
    var errorMsg: String {
        get { _errorMsg ?? "Maximum \(maxLength) characters"}
        set { _errorMsg = newValue }
    }
    var shouldTrim: Bool = false
    
    public func validate(text: String) -> Bool {
        var _text = text
        if shouldTrim {
            _text = text.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        
        return _text.count <= maxLength
    }
    
    public func getErrorMessage() -> String {
        return errorMsg
    }
    
    public mutating func setError(msg: String) {
        self.errorMsg = msg
    }
}

public struct IsNumberRule: BaseRule {
    var errorMsg: String = "Invalid number"
    var shouldTrim: Bool = false
    
    public func validate(text: String) -> Bool {
        var _text = text
        if shouldTrim {
            _text = text.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        
        if _text.isEmpty {
            return false
        }
        
        return Double(text) != nil
    }
    
    public func getErrorMessage() -> String {
        return errorMsg
    }
    
    public mutating func setError(msg: String) {
        self.errorMsg = msg
    }
}

public struct GreaterThanRule: BaseRule {
    var _errorMsg: String? = nil
    var errorMsg: String {
        get { _errorMsg ?? "Should be greater than  \(target)"}
        set { _errorMsg = newValue }
    }
    var target: NSNumber
    var shouldTrim: Bool = false
    
    public func validate(text: String) -> Bool {
        var _text = text
        if shouldTrim {
            _text = text.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        
        
        if let value = Double(_text), value > target.doubleValue {
            return true
        } else {
            return false
        }
    }
    
    public func getErrorMessage() -> String {
        return errorMsg
    }
    
    public mutating func setError(msg: String) {
        self.errorMsg = msg
    }
}

public struct LessThanRule: BaseRule {
    var _errorMsg: String? = nil
    var target: NSNumber
    var errorMsg: String {
        get { _errorMsg ?? "Should be greater than  \(target)"}
        set { _errorMsg = newValue }
    }
    var shouldTrim: Bool = false
    
    public func validate(text: String) -> Bool {
        var _text = text
        if shouldTrim {
            _text = text.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        
        
        if let value = Double(_text), value < target.doubleValue {
            return true
        } else {
            return false
        }
    }
    
    public func getErrorMessage() -> String {
        return errorMsg
    }
    
    public mutating func setError(msg: String) {
        self.errorMsg = msg
    }
}

public struct NoWhitespaceRule: BaseRule {
    var errorMsg: String = "Whitespaces are not allowed"
    var shouldTrim: Bool = false
    
    public func validate(text: String) -> Bool {
        var _text = text
        if shouldTrim {
            _text = text.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        
        return !_text.contains(" ")
    }
    
    public func getErrorMessage() -> String {
        return errorMsg
    }
    
    public mutating func setError(msg: String) {
        self.errorMsg = msg
    }
}
