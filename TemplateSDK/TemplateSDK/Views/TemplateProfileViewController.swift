//
//  TemplateProfileViewController.swift
//  TemplateSDK
//
//  Created by Admin on 06/04/21.
//

import Foundation
import UIKit


public class TemplateProfile {
    public static var getProfileUrl: String!
    public static var updateProfileUrl: String!
    
    public static func loadFromNib(
        getProfileUrl: String? = nil,
        updateProfileUrl: String? = nil,
        successProfileResult: ((_ success: Bool, _ result: Any?) -> Void)? = nil
    ) -> TemplateProfileViewController {
        let vc = TemplateUtils.mainStoryboard.instantiateViewController(withIdentifier: "TemplateProfileViewController") as! TemplateProfileViewController
        
        vc.successProfileResult = successProfileResult
        
        if let getProfileUrl = getProfileUrl {
            TemplateProfile.getProfileUrl = getProfileUrl
        }
        
        if let updateProfileUrl = updateProfileUrl {
            TemplateProfile.updateProfileUrl = updateProfileUrl
        }
        
        return vc
    }
}

public class TemplateProfileUI: BaseViewController {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBack: TemplateButton!
    @IBOutlet weak var titleContainer: UIView!
    @IBOutlet weak var fieldUsernameLabel: UILabel!
    @IBOutlet weak var fieldUsername: TemplateField!
    @IBOutlet weak var fieldNameLabel: UILabel!
    @IBOutlet weak var fieldName: TemplateField!
    @IBOutlet weak var fieldPasswordLabel: UILabel!
    @IBOutlet weak var fieldPassword: TemplateField!
    @IBOutlet weak var childViewContainer: UIView!
    @IBOutlet weak var btnSubmit: TemplateButton!
    
}

public class TemplateProfileViewController: BaseViewController {
    public weak var templateProfileUI: TemplateProfileUI!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    public var isEmailField: Bool = false
    public var fieldLabelColor: UIColor?
    
    public var successProfileResult: ((_ success: Bool, _ result: Any?) -> Void)? = nil
    public lazy var user: TemplateUser? = {
        return TemplateUserDefaults.sharedInstance.user
    }()
    public var storeUser: Bool = true
    
    public var enableLoadingView: Bool = true
    public var closeViewWhenUpdate: Bool = false

    var profileUpdated: Bool = false
    
    var isValidFields = true
    var fieldErrors: [(String, String)] = []
    
    public lazy var errorCallback: ((_ fieldName: String, _ errorMessage: String) -> Void) = {
        { (fieldName, errorMessage) in
            self.fieldErrors.append((fieldName, errorMessage))
            self.isValidFields = false
        }
    }()
    
    public lazy var usernameValidator: Validator? = {
        let validator = Validator(text: "").nonEmpty()
        var fieldName = "Username"
        if isEmailField {
            let _ = validator.validEmail()
            fieldName = "Email"
        }
        
        return validator.addErrorCallback { (errorMessage) in
            self.errorCallback(fieldName, errorMessage)
        }
    }()
    public lazy var nameValidator: Validator? = {
        Validator(text: "").nonEmpty().addErrorCallback{ (errorMessage) in
            self.errorCallback("Name", errorMessage)
        }
    }()
    public lazy var passwordValidator: Validator? = {
        Validator(text: "").addErrorCallback{ (errorMessage) in
            self.errorCallback("Password", errorMessage)
        }
    }()
    
    public lazy var validators: [String: Validator] = {
        var validators: [String: Validator] = [:]
        
        validators["username"] = usernameValidator
        validators["name"] = nameValidator
        validators["password"] = passwordValidator
        
        
        return validators
    }()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        setUpView()
        populateView()
        callGetProfile()
    }
    
    public func populateView() {
        templateProfileUI.fieldName.text = user?.name ?? ""
        templateProfileUI.fieldUsername.text = user?.email ?? ""
    }
    
    public func setUpView() {
        self.title = templateProfileUI.lblTitle.text
        removeBackButtonText()
        
        if isEmailField {
            templateProfileUI.fieldUsername.placeholder = "Enter email"
            templateProfileUI.fieldUsernameLabel.text = "Email"
        } else {
            templateProfileUI.fieldUsername.placeholder = "Enter username"
            templateProfileUI.fieldUsernameLabel.text = "Username"
        }

        if let fieldLabelColor = fieldLabelColor {
            templateProfileUI.fieldUsernameLabel.textColor = fieldLabelColor
            templateProfileUI.fieldPasswordLabel.textColor = fieldLabelColor
            templateProfileUI.fieldNameLabel.textColor = fieldLabelColor
        }
        
        templateProfileUI.btnSubmit.delegate = self
        templateProfileUI.btnBack.delegate = self
    }
    
    public func validateFields(_ showAlert: Bool = true) -> Bool {
        fieldErrors = []
        isValidFields = true
        
        if let usernameValidator = usernameValidator {
            let _ = templateProfileUI.fieldUsername.setValidator(validator: usernameValidator)
        }
        
        if let nameValidator = nameValidator {
            let _ = templateProfileUI.fieldName.setValidator(validator: nameValidator)
        }
        
        if let passwordValidator = passwordValidator {
            let _ = templateProfileUI.fieldPassword.setValidator(validator: passwordValidator)
        }
        
        for (_, value) in validators {
            let _ = value.validate()
        }
        
        
        if !isValidFields, showAlert {
            TemplateUtils.showAlert(from: fieldErrors, presentIn: self)
        }
        
        return isValidFields
    }
    
    open func callGetProfile() {
        callGetProfile(type: TemplateUser.self)
    }
    
    open func callGetProfile<T: Codable>(type: T.Type) {
        if enableLoadingView {
            self.showIndicator()
        }
        
        TemplateUser.getProfile(
            type: type,
            url: TemplateInstance.API_URL + TemplateProfile.getProfileUrl,
            success: { user in
                print("user = \(user)")
                
                if self.storeUser {
                    do { try TemplateUserDefaults.setUser(user: user) }
                    catch { print("Couldn't store user") }
                }
                
                if self.enableLoadingView {
                    self.hideIndicator()
                }
                
                if self.profileUpdated {
                    self.successProfileResult?(true, user)
                    
                    if self.closeViewWhenUpdate {
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        let alert = UIAlertController(title: "Profile Update", message: "Profile updated successfully", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
                            
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    self.profileUpdated = false
                }
            },
            failure: { message, error in
                if self.enableLoadingView {
                    self.hideIndicator()
                }
                
                if let message = message {
                    let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                } else if let error = error {
                    print(error)
                }
            }
        )
    }
    
    open func callUpdateProfile<T: Codable>(type: T.Type) {
        if !validateFields() {
            return
        }
        
        var username: String? = nil
        if let _username = templateProfileUI.fieldUsername.text, !_username.isEmpty {
            username = _username
        }
        
        var name: String? = nil
        if let _name = templateProfileUI.fieldName.text, !_name.isEmpty {
            name = _name
        }
        
        var password: String? = nil
        if let _password = templateProfileUI.fieldPassword.text, !_password.isEmpty {
            password = _password
        }
        
        if enableLoadingView {
            self.showIndicator()
        }
        
        TemplateUser.updateProfile(
            url: TemplateInstance.API_URL + TemplateProfile.updateProfileUrl,
            name: name,
            password: password,
            email: username,
            success: {
                if self.enableLoadingView {
                    self.hideIndicator()
                }
                
                self.profileUpdated = true
                
                self.callGetProfile()
            },
            failure: { message, error in
                print("Update profile error")
                if self.enableLoadingView {
                    self.hideIndicator()
                }
                
                if let message = message {
                    let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                } else if let error = error {
                    print(error)
                }
            }
        )
    }
    
    open func callUpdateProfile() {
        callUpdateProfile(type: TemplateUser.self)
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "TemplateProfileUISegue" {
            templateProfileUI = (segue.destination as! TemplateProfileUI)
            
            if let hideNavBar = self.hideNavBar {
                templateProfileUI.titleContainer?.isHidden = !hideNavBar
            }
        }
    }
}

extension TemplateProfileViewController: TemplateButtonDelegate {
    public func clickHandler(_ button: TemplateButton) {
        switch button {
        case templateProfileUI.btnSubmit:
            callUpdateProfile()
            break
        case templateProfileUI.btnBack:
            navigationController?.popViewController(animated: true)
        default:
            break
        }
    }
}
