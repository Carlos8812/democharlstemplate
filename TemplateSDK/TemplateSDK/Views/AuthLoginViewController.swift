//
//  AuthLoginViewController.swift
//  TemplateSDK
//
//  Created by Admin on 29/03/21.
//

import Foundation
import UIKit

public struct AuthLogin {
    public static var loginUrl: String!
    
    public static func loadFromNib(
        loginUrl: String? = nil,
        successLoginResult: ((_ success: Bool, _ result: Any?) -> UIViewController?)? = nil
    ) -> AuthLoginViewController {
        let vc = TemplateUtils.mainStoryboard.instantiateViewController(withIdentifier: "AuthLoginViewController") as! AuthLoginViewController
        
        vc.successLoginResult = successLoginResult
        
        if let loginUrl = loginUrl {
            AuthLogin.loginUrl = loginUrl
        }
        
        return vc
    }
}

open class AuthLoginUI: BaseViewController {
    @IBOutlet weak var lblTitle: UILabel?
    @IBOutlet weak var btnBack: TemplateButton?
    @IBOutlet weak var titleContainer: UIView?
    @IBOutlet weak var lblFieldUsername: UILabel?
    @IBOutlet weak var fieldUsername: UITextField!
    @IBOutlet weak var lblFieldPassword: UILabel?
    @IBOutlet weak var fieldPassword: UITextField!
    @IBOutlet weak var btnForgotPassword: TemplateButton?
    @IBOutlet weak var btnGoToSignUp: TemplateButton?
    @IBOutlet weak var btnLogin: TemplateButton!
    @IBOutlet weak var scrollView: TemplateScrollView!

}

open class AuthLoginViewController: BaseViewController {
    open var authLoginUI: AuthLoginUI!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var contentView: UIView!
    
    public var isEmailField: Bool = false
    public var fieldLabelColor: UIColor?
    public var clickLabelColor: UIColor?
    public var enableForgotPassword: Bool = true
    public var enableGoToSignUp: Bool = true

    public var successLoginResult: ((_ success: Bool, _ result: Any?) -> UIViewController?)?
    public var storeUser: Bool = true

    public var enableLoadingView: Bool = true
    
    var isValidFields = true
    var fieldErrors: [(String, String)] = []

    public lazy var errorCallback: ((_ fieldName: String, _ errorMessage: String) -> Void) = {
        { (fieldName, errorMessage) in
            self.fieldErrors.append((fieldName, errorMessage))
            self.isValidFields = false
        }
    }()

    public lazy var usernameValidator: Validator? = {
        let validator = Validator(text: "").nonEmpty()
        var fieldName = "Username"
        if isEmailField {
            let _ = validator.validEmail()
            fieldName = "Email"
        }

        return validator.addErrorCallback { (errorMessage) in
            self.errorCallback(fieldName, errorMessage)
        }
    }()
    public lazy var passwordValidator: Validator? = {
        Validator(text: "").nonEmpty().addErrorCallback{ (errorMessage) in
            self.errorCallback("Password", errorMessage)
        }
    }()

    public lazy var validators: [String: Validator] = {
        var validators: [String: Validator] = [:]

        validators["username"] = usernameValidator
        validators["password"] = passwordValidator


        return validators
    }()
    
    
    open override func loadView() {
        super.loadView()
        
        if authLoginUI == nil {
            authLoginUI = (TemplateUtils.mainStoryboard.instantiateViewController(withIdentifier: "AuthLoginUI") as! AuthLoginUI)
        }
        
        authLoginUI.view.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(authLoginUI.view)
        
        NSLayoutConstraint.activate([
            authLoginUI.view.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
            authLoginUI.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0),
            authLoginUI.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0),
            authLoginUI.view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0)
        ])
    }


    override public func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    public func setUpView() {
        guard let authLoginUI = authLoginUI else { return }

        if let title = authLoginUI.lblTitle?.text {
            self.title = title
        } else {
            self.title = "LOGIN"
        }
        removeBackButtonText()
        
        if let hideNavBar = self.hideNavBar {
            authLoginUI.titleContainer?.isHidden = !hideNavBar
        }

        if isEmailField {
            authLoginUI.fieldUsername.placeholder = "EMAIL"
            authLoginUI.fieldUsername.keyboardType = .emailAddress
            authLoginUI.fieldUsername.textContentType = .emailAddress
            authLoginUI.lblFieldUsername?.text = "Email"
        } else {
            authLoginUI.fieldUsername.placeholder = "Enter username"
            authLoginUI.fieldUsername.keyboardType = .default
            authLoginUI.fieldUsername.textContentType = .username
            authLoginUI.lblFieldUsername?.text = "Username"
        }

        if let fieldLabelColor = fieldLabelColor {
            authLoginUI.lblFieldUsername?.textColor = fieldLabelColor
            authLoginUI.lblFieldPassword?.textColor = fieldLabelColor
        }

        if let clickLabelColor = clickLabelColor {
            authLoginUI.btnForgotPassword?.setTitleColor(clickLabelColor, for: .normal)
            authLoginUI.btnGoToSignUp?.setTitleColor(clickLabelColor, for: .normal)
        }

        authLoginUI.btnForgotPassword?.isHidden = !enableForgotPassword
        authLoginUI.btnGoToSignUp?.isHidden = !enableGoToSignUp

        authLoginUI.btnForgotPassword?.delegate = self
        authLoginUI.btnGoToSignUp?.delegate = self
        authLoginUI.btnLogin.delegate = self
        authLoginUI.btnBack?.delegate = self
    }

    public func validateFields(_ showAlert: Bool = true) -> Bool {
        guard let authLoginUI = authLoginUI else { return false }

        fieldErrors = []
        isValidFields = true

        if let usernameValidator = usernameValidator {
            let _ = authLoginUI.fieldUsername.setValidator(validator: usernameValidator)
        }

        if let passwordValidator = passwordValidator {
            let _ = authLoginUI.fieldPassword.setValidator(validator: passwordValidator)
        }

        for (_, value) in validators {
            let _ = value.validate()
        }


        if !isValidFields, showAlert {
            TemplateUtils.showAlert(from: fieldErrors, presentIn: self)
        }

        return isValidFields
    }

    open func callLogin<T: Codable>(type: T.Type) {
        guard let authLoginUI = authLoginUI else { return }

        if !validateFields() {
            return
        }

        if self.enableLoadingView {
            self.showIndicator()
        }

        TemplateUser.login(
            type: type,
            url: TemplateInstance.API_URL + AuthLogin.loginUrl,
            email: authLoginUI.fieldUsername.text!,
            password: authLoginUI.fieldPassword.text!,
            deviceToken: TemplateUserDefaults.sharedInstance.deviceToken,
            success: { user in
                print("user = \(user)")
                if self.storeUser {
                    do { try TemplateUserDefaults.setUser(user: user) }
                    catch { print("Store user failed") }
                }

                if self.enableLoadingView {
                    self.hideIndicator()
                }

                if let successLoginResult = self.successLoginResult, let vc = successLoginResult(true, user) {
                    self.navigationController?.setViewControllers([vc], animated: true)
                } else {
                    self.navigationController?.setViewControllers(
                        [TemplateInstance.instanceTutorialVC?() ?? TemplateDashboard.loadFromNib()],
                        animated: true
                    )
                }

            },
            failure: { message, error in
                if self.enableLoadingView {
                    self.hideIndicator()
                }

                if let message = message {
                    let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in

                    }))

                    self.present(alert, animated: true, completion: nil)
                } else if let error = error {
                    print(error)
                }
            }
        )
    }

    open func callLogin() {
        callLogin(type: TemplateUser.self)
    }

//    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "AuthLoginUISegue" {
//            authLoginUI = (segue.destination as! AuthLoginUI)
//
//            if let hideNavBar = self.hideNavBar {
//               authLoginUI!.titleContainer?.isHidden = !hideNavBar
//            }
//        }
//    }
}

extension AuthLoginViewController: TemplateButtonDelegate {
    public func clickHandler(_ button: TemplateButton) {
        guard let authLoginUI = authLoginUI else { return }

        switch button {
        case authLoginUI.btnForgotPassword:
            navigationController?.pushViewController(
                TemplateInstance.instanceRecoveryVC?() ?? AuthRecovery.loadFromNib(),
                animated: true
            )
            break
        case authLoginUI.btnGoToSignUp:
            navigationController?.pushViewController(
                TemplateInstance.instanceSignUpVC?() ?? AuthSignUp.loadFromNib(),
                animated: true
            )
            break
        case authLoginUI.btnLogin:
            callLogin()
            break
        case authLoginUI.btnBack:
            navigationController?.popViewController(animated: true)
        default:
            break
        }
    }
}
