//
//  TemplateDashboard.swift
//  TemplateSDK
//
//  Created by Admin on 07/04/21.
//

import Foundation
import UIKit

public class TemplateDashboard {
    
    public static func loadFromNib(_ showSliderButton: Bool? = nil, _ sliderSide: SliderSide? = nil) -> TemplateDashboardViewController {
        let vc = TemplateUtils.mainStoryboard.instantiateViewController(withIdentifier: "TemplateDashboardViewController") as! TemplateDashboardViewController
        
        if let showSliderButton = showSliderButton {
            vc.showSliderButton = showSliderButton
        }
        
        if let sliderSide = sliderSide {
            vc.sliderSide = sliderSide
        }
        
        return vc
    }
}


public class TemplateDashboardViewController: BaseViewController {
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var contentView: UIView? {
        didSet {
            setConstraintsToChildView(childVC: _childVC, contentView: contentView)
        }
    }
    
    public var showSliderButton: Bool = true
    public lazy var sliderButtonImage: UIImage? = {
        return UIImage(named: "icSlider", in: TemplateUtils.resourceBundle, compatibleWith: nil)
    }()
    public var sliderSide: SliderSide = .left
    
    private var _childVC: UIViewController? {
        didSet {
            if let childVC = _childVC {
                addChild(childVC)
                childVC.view.translatesAutoresizingMaskIntoConstraints = false
                
                setConstraintsToChildView(childVC: childVC, contentView: contentView)
            }
        }
    }
    
    open var childView: UIViewController? {
        set {
            _childVC?.willMove(toParent: nil)
            _childVC?.view.removeFromSuperview()
            _childVC?.removeFromParent()
            _childVC = newValue
        }
        get { _childVC }
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpView()
    }
    
    open func setUpView() {
        removeBackButtonText()
        
        if let image = sliderButtonImage {
            
            let button = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(sliderButtonClicked))
            
            SliderViewController.sharedInstance.sliderSide = sliderSide
            if sliderSide == .right {
                self.navigationItem.rightBarButtonItem = button
            } else {
                self.navigationItem.leftBarButtonItem = button
            }
        }
    }
    
    open func setEmbeddedView(_ viewController: UIViewController) {
        childView = viewController
    }
    
    @objc open func sliderButtonClicked() {
        SliderViewController.sharedInstance.toggle()
    }
    
    open func setConstraintsToChildView(childVC: UIViewController?, contentView: UIView?) {
        guard let contentView = contentView else {
            return
        }
        
        guard let childVC = childVC else {
            return
        }
        
        contentView.addSubview(childVC.view)
        
        NSLayoutConstraint.activate([
            childVC.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0),
            childVC.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0),
            childVC.view.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
            childVC.view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0),
        ])
        
        childVC.didMove(toParent: self)
    }
}
