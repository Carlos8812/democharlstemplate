//
//  TemplateSliderMenuViewController.swift
//  TemplateSDK
//
//  Created by Admin on 07/04/21.
//

import Foundation
import UIKit
import MessageUI

open class TemplateSliderMenuViewController: BaseViewController {
    @IBOutlet public weak var table: UITableView!
    
    public lazy var rows: [Any] = {
        []
    }()
    
    public weak var delegate: TemplateSliderMenuDelegate?
    public weak var sliderViewController: SliderViewController?
    
    public static var SHARE_APP_LINK = ""
    public var shareAppLink: String {
        get { return TemplateSliderMenuViewController.SHARE_APP_LINK }
        set { TemplateSliderMenuViewController.SHARE_APP_LINK = newValue }
    }
    
    public static func loadFromNib(
        _ slider: SliderViewController? = nil,
        _ defaultRows: [Any]? = nil,
        _ showLogout: Bool = true,
        _ showProfile: Bool = true,
        _ showNotifications: Bool = true,
        _ showPrivacyPolicy: Bool = true,
        _ showAbout: Bool = true,
        _ showShareApp: Bool = true
    ) -> TemplateSliderMenuViewController {
        let vc = TemplateUtils.mainStoryboard.instantiateViewController(withIdentifier: "TemplateSliderMenuViewController") as! TemplateSliderMenuViewController
        
        vc.sliderViewController = slider
        
        if showProfile {
            let menu = TemplateSliderMenu(text: "Profile")
            menu.clickHandler = { _, _ in
                (UIApplication.shared.keyWindow?.rootViewController as? UINavigationController)?.pushViewController(
                    TemplateInstance.instanceProfileVC?() ?? TemplateProfile.loadFromNib(), animated: true
                )
            }
            vc.rows.append(menu)
        }
        
        if showNotifications {
            func showNotificationCouldntInit() {
                let alert = UIAlertController(
                    title: nil,
                    message: "Couldn't enable notifications, go to system settings and allow notifications",
                    preferredStyle: .alert
                )
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "Go to Settings", style: .default, handler: { (_) in
                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                        return
                    }
                    
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                print("Settings opened: \(success)") // Prints true
                            })
                        } else {
                            UIApplication.shared.openURL(settingsUrl as URL)
                        }
                    }
                }))
                
                vc.present(alert, animated: true, completion: nil)
            }

            
            let menu = TemplateSliderMenu(text: "Notifications", closeSliderOnClick: false)
            var initNotificationsTries = 0
            
            if TemplateUserDefaults.getNotificationStatus() == nil {
                TemplateUtils.isNotificationsAuthorized { (granted) in
                    vc.table.reloadData()
                }
            }
            
            menu.clickHandler = { tableView, indexPath in
                if TemplateUserDefaults.getNotificationStatus() == true {
                    TemplateUtils.disableNotifications()
                    tableView.reloadData()
                } else {
                    tableView.reloadData()
                    
                    TemplateUtils.initNotifications(nil) { granted in
                        if granted {
                            initNotificationsTries = 0
                        } else {
                            initNotificationsTries += 1
                            
                            if initNotificationsTries > 1 {
                                showNotificationCouldntInit()
                            }
                        }
                        
                        tableView.reloadData()
                    }
                }
            }
            menu.getTableViewCell = { tableView, indexPath in
                var cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCustomCell") as? NotificationCustomCell
                
                if cell == nil {
                    tableView.register(UINib(nibName: "NotificationCustomCell", bundle: TemplateUtils.resourceBundle), forCellReuseIdentifier: "NotificationCustomCell")
                    cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCustomCell") as? NotificationCustomCell
                }
                
                if let cell = cell  {
                    cell.lblText.text = menu.text
                    cell.switchNotification.setOn(TemplateUserDefaults.getNotificationStatus() == true, animated: true)
                    
                    return cell
                }
                
                return nil
            }
            
            vc.rows.append(menu)
        }
        
        if showPrivacyPolicy {
            let menu = TemplateSliderMenu(text: "Privacy Policy")
            
            menu.clickHandler = { _, _ in
                (UIApplication.shared.keyWindow?.rootViewController as? UINavigationController)?.pushViewController(
                    TemplateInstance.instancePrivacyPolicyVC?() ?? TemplatePrivacyPolicy.loadFromNib(), animated: true
                )
            }
            
            vc.rows.append(menu)
        }
        
        if showAbout {
            let menu = TemplateSliderMenu(text: "About")
            
            menu.clickHandler = { _, _ in
                (UIApplication.shared.keyWindow?.rootViewController as? UINavigationController)?.pushViewController(
                    TemplateInstance.instanceAboutVC?() ?? TemplateAbout.loadFromNib(), animated: true
                )
            }
            
            vc.rows.append(menu)
        }
        
        if showShareApp {
            let menu = TemplateSliderMenu(text: "Share App")
            
            
            menu.clickHandler = { _, _ in
                if !MFMessageComposeViewController.canSendText() {
                    print("SMS not available")
                    return
                }
                
                let composeVC = MFMessageComposeViewController()
                composeVC.messageComposeDelegate = vc
                composeVC.body = "Body check out this app\n\(SHARE_APP_LINK)" // TODO: Link
                
                vc.present(composeVC, animated: true, completion: nil)
            }
            
            vc.rows.append(menu)
        }
        
        if showLogout {
            let menu = TemplateSliderMenu(text: "Logout")
            
            menu.clickHandler = { _, _ in
                TemplateUserDefaults.removeUser()
            }
            
            vc.rows.append(menu)
        }
        
        if let defaultRows = defaultRows {
            vc.rows += defaultRows
        }
        
        return vc
    }
}

extension TemplateSliderMenuViewController: UITableViewDelegate, UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return delegate?.getCustomCellHeight(tableView, heightForRowAt: indexPath) ?? 71
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if
            let row = rows[indexPath.row] as? TemplateSliderMenu,
            let cell = row.getTableViewCell?(tableView, indexPath)
        {
            return cell
        } else if
            let cell = tableView.dequeueReusableCell(withIdentifier: "TemplateSliderMenuCell") as? TemplateSliderMenuCell,
            let row = rows[indexPath.row] as? TemplateSliderMenu,
            !row.isOverriden
        {
            cell.imgIcon.image = row.image
            cell.lblText.text = row.text
            
            return cell
        } else if let cell = delegate?.getCustomCell(tableView, cellForRowAt: indexPath, rows: rows) {
            return cell
        }
        
        fatalError("TemplateSliderMenuViewController cell type not found")
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = rows[indexPath.row]
        
        if let row = row as? TemplateSliderMenu{
            if row.closeSliderOnClick {
                sliderViewController?.close()
            }
            
            row.clickHandler?(tableView, indexPath)
        }
        
        delegate?.rowClickHandler(row)
    }
}

extension TemplateSliderMenuViewController: MFMessageComposeViewControllerDelegate {
    public func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
}

open class TemplateSliderMenuCell: UITableViewCell {
    @IBOutlet public weak var lblText: UILabel!
    @IBOutlet public weak var imgIcon: UIImageView!
    
}

open class TemplateSliderMenu {
    public var text: String
    public var image: UIImage? = nil
    public var isOverriden: Bool = false
    public var clickHandler: ((_ tableView: UITableView, _ indexPath: IndexPath) -> Void)? = nil
    public var closeSliderOnClick: Bool = true
    public var getTableViewCell: ((_ tableView: UITableView, _ indexPath: IndexPath) -> UITableViewCell?)? = nil
    
    public init(
        text: String,
        image: UIImage? = nil,
        isOverriden: Bool = false,
        closeSliderOnClick: Bool = true,
        clickHandler: ((_ tableView: UITableView, _ indexPath: IndexPath) -> Void)? = nil,
        getTableViewCell: ((_ tableView: UITableView, _ indexPath: IndexPath) -> UITableViewCell?)? = nil
    ) {
        self.text = text
        self.image = image
        self.isOverriden = isOverriden
        self.clickHandler = clickHandler
        self.closeSliderOnClick = closeSliderOnClick
        self.getTableViewCell = getTableViewCell
    }
}

public protocol TemplateSliderMenuDelegate: AnyObject {
    func getCustomCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, rows: [Any]) -> UITableViewCell?
    
    func getCustomCellHeight(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat?
    
    func rowClickHandler(_ row: Any)
}
