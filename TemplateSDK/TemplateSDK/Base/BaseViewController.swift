//
//  BaseViewController.swift
//  TemplateSDK
//
//  Created by Admin on 25/05/21.
//

import Foundation
import UIKit

open class BaseViewController: UIViewController {
    @IBInspectable public var enableDefaultStyles: Bool {
        get {
            return setStyles
        }
        set {
            setStyles = newValue
        }
    }
    
    open var setStyles: Bool = true
    
    open var hideNavBar: Bool?
    public var previousNavBarHiddenStatus: Bool?
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpStyleView()
    }
    
    open func setUpStyleView() {
        if setStyles {
            let styles = TemplateInstance.templateStyles!
            
            if let color = styles.backgroundColor {
                self.view.backgroundColor = color
            }
            
//            if let color = styles.navBarColor {
//                self.navigationController?.navigationBar.barTintColor = color
//            }
//
//            if let color = styles.navBarItemsColor {
//                let navBar = self.navigationController?.navigationBar
//                navBar?.tintColor = color
//
//                var titleAttrs = navBar?.titleTextAttributes ?? [:]
//                titleAttrs[.foregroundColor] = color
//                navBar?.titleTextAttributes = titleAttrs
//            }
//
//            if let font = styles.navBarTextFont {
//                let navBar = self.navigationController?.navigationBar
//
//                var titleAttrs = navBar?.titleTextAttributes ?? [:]
//                titleAttrs[.font] = font
//                navBar?.titleTextAttributes = titleAttrs
//            }
        }
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        previousNavBarHiddenStatus = self.navigationController?.isNavigationBarHidden
        
        if let hideNavBar = hideNavBar {
            self.navigationController?.setNavigationBarHidden(hideNavBar, animated: false)
        }
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let previousNavBarHiddenStatus = previousNavBarHiddenStatus {
            self.navigationController?.setNavigationBarHidden(previousNavBarHiddenStatus, animated: false)
        }
    }
}
