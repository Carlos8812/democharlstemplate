//
//  Template.swift
//  TemplateSDK
//
//  Created by Admin on 30/03/21.
//

import Foundation
import UIKit

public protocol TemplateStylesDelegate {
    var backgroundColor: UIColor? { get }
    var textOnBackgroundColor: UIColor? { get }
    var editTextFont: UIFont? { get }
    
    var navBarColor: UIColor? { get }
    var navBarItemsColor: UIColor? { get }
    var navBarTextFont: UIFont? { get }
    
    var buttonStyle: TemplateButtonStyle? { get }
    var buttonBorderLessStyle: TemplateButtonStyle? { get }
    
    var pageControlStyle: TemplatePageControlStyle? { get }
    
    var labelTitleStyle: TemplateLabelStyle? { get }
    var labelLargeStyle: TemplateLabelStyle? { get }
    var labelMediumStyle: TemplateLabelStyle? { get }
    var labelSmallStyle: TemplateLabelStyle? { get }
    var labelFieldNameStyle: TemplateLabelStyle? { get }
}

extension TemplateStylesDelegate {
    public func getColorFromAssets(_ name: String) -> UIColor? {
        return UIColor(named: name)
    }
}

public class TemplateInstance {
    public static var API_URL = ""
    public static var UPDATE_DEVICE_TOKEN_URL: String = ""
    
    public static var instanceAuthSelectorVC: (() -> AuthSelectorViewController)?
    public static var instanceLoginVC: (() -> AuthLoginViewController)?
    public static var instanceSignUpVC: (() -> AuthSignUpViewController)?
    public static var instanceRecoveryVC: (() -> AuthRecoveryViewController)?
    public static var instanceDashboardVC: (() -> TemplateDashboardViewController)?
    public static var instanceProfileVC: (() -> TemplateProfileViewController)?
    public static var instancePrivacyPolicyVC: (() -> TemplatePrivacyPolicyViewController)?
    public static var instanceAboutVC: (() -> TemplateAboutViewController)?
    public static var instanceTutorialVC: (() -> TemplateTutorialViewController)?
    public static var templateStyles: TemplateStylesDelegate!
    
    public static func initialize(
        styles: TemplateStylesDelegate,
        apiUrl: String,
        loginUrl: String,
        signUpUrl: String,
        recoveryUrl: String,
        getProfileUrl: String,
        updateProfileUrl: String,
        updateDeviceTokenUrl: String,
        instanceAuthSelectorVC: (() -> AuthSelectorViewController)? = nil,
        instanceLoginVC: (() -> AuthLoginViewController)? = nil,
        instanceSignUpVC: (() -> AuthSignUpViewController)? = nil,
        instanceRecoveryVC: (() -> AuthRecoveryViewController)? = nil,
        instanceDashboardVC: (() -> TemplateDashboardViewController)? = nil,
        instanceProfileVC: (() -> TemplateProfileViewController)? = nil,
        instancePrivacyPolicyVC: (() -> TemplatePrivacyPolicyViewController)? = nil,
        instanceAboutVC: (() -> TemplateAboutViewController)? = nil,
        instanceTutorialVC: (() -> TemplateTutorialViewController)? = nil
    ) {
        templateStyles = styles
        TemplateInstance.API_URL = apiUrl
        AuthLogin.loginUrl = loginUrl
        AuthSignUp.signUpUrl = signUpUrl
        AuthRecovery.recoveryUrl = recoveryUrl
        TemplateProfile.getProfileUrl = getProfileUrl
        TemplateProfile.updateProfileUrl = updateProfileUrl
        TemplateInstance.UPDATE_DEVICE_TOKEN_URL = updateDeviceTokenUrl
        
        TemplateInstance.instanceAuthSelectorVC = instanceAuthSelectorVC
        TemplateInstance.instanceLoginVC = instanceLoginVC
        TemplateInstance.instanceSignUpVC = instanceSignUpVC
        TemplateInstance.instanceRecoveryVC = instanceRecoveryVC
        TemplateInstance.instanceDashboardVC = instanceDashboardVC
        TemplateInstance.instanceProfileVC = instanceProfileVC
        TemplateInstance.instancePrivacyPolicyVC = instancePrivacyPolicyVC
        TemplateInstance.instanceAboutVC = instanceAboutVC
        TemplateInstance.instanceTutorialVC = instanceTutorialVC
    }
}
