//
//  TemplateLabel.swift
//  TemplateSDK
//
//  Created by Admin on 27/05/21.
//

import Foundation
import UIKit

open class TemplateLabel: UILabel {
    
    @IBInspectable public var setDefaultStyle: Bool {
        get {
            return _setDefaultStyle
        }
        set {
            _setDefaultStyle = newValue
            setUp()
        }
    }
    
    private var _setDefaultStyle: Bool = true
    
    open var style: TemplateLabelStyle? {
        didSet {
            setUp()
        }
    }


    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUp()
    }

    init() {
        super.init(frame: CGRect.zero)
        setUp()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }

    public func setUp() {
        if let tmpStyle = style, _setDefaultStyle == true {

            if let color = tmpStyle.textColor {
                self.textColor = color
            }

            if let font = tmpStyle.textFont {
                self.font = font
            }
        }
    }
}

public class TemplateLabelStyle {
    public var textColor: UIColor?
    public var textFont: UIFont?
    
    public init(
        textColor: UIColor? = nil,
        textFont: UIFont? = nil
    ) {
        self.textColor = textColor
        self.textFont = textFont
    }
}


open class TitleTemplateLabel: TemplateLabel {
    private var _style: TemplateLabelStyle? = nil
    
    open override var style: TemplateLabelStyle? {
        get {
            return _style ?? TemplateInstance.templateStyles.labelTitleStyle
        }
        set {
            _style = newValue
        }
    }
}

open class LargeTemplateLabel: TemplateLabel {
    private var _style: TemplateLabelStyle? = nil
    
    open override var style: TemplateLabelStyle? {
        get {
            return _style ?? TemplateInstance.templateStyles.labelLargeStyle
        }
        set {
            _style = newValue
        }
    }
}

open class MediumTemplateLabel: TemplateLabel {
    private var _style: TemplateLabelStyle? = nil
    
    open override var style: TemplateLabelStyle? {
        get {
            return _style ?? TemplateInstance.templateStyles.labelMediumStyle
        }
        set {
            _style = newValue
        }
    }
}

open class SmallTemplateLabel: TemplateLabel {
    private var _style: TemplateLabelStyle? = nil
    
    open override var style: TemplateLabelStyle? {
        get {
            return _style ?? TemplateInstance.templateStyles.labelSmallStyle
        }
        set {
            _style = newValue
        }
    }
}

open class FieldNameTemplateLabel: TemplateLabel {
    private var _style: TemplateLabelStyle? = nil
    
    open override var style: TemplateLabelStyle? {
        get {
            return _style ?? TemplateInstance.templateStyles.labelFieldNameStyle
        }
        set {
            _style = newValue
        }
    }
}
