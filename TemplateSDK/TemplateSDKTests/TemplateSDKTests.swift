//
//  TemplateSDKTests.swift
//  TemplateSDKTests
//
//  Created by Admin on 26/03/21.
//

import XCTest
@testable import TemplateSDK

class TemplateSDKTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testNonEmptyRule() throws {
        XCTAssertTrue(NonEmptyRule().validate(text: "correct text"))
        XCTAssertTrue(NonEmptyRule().validate(text: "."))
        XCTAssertTrue(NonEmptyRule().validate(text: "  .   "))
        XCTAssertTrue(NonEmptyRule(shouldTrim: false).validate(text: " "))
        
        XCTAssertFalse(NonEmptyRule().validate(text: ""))
        XCTAssertFalse(NonEmptyRule().validate(text: "     "))
        XCTAssertFalse(NonEmptyRule().validate(text: "\n"))
    }
    
    func testEmailValidationCorrect() throws {
        XCTAssertTrue(EmailRule().validate(text: "true@email.com"))
        XCTAssertTrue(EmailRule().validate(text: "true@email.co.uk"))
        XCTAssertTrue(EmailRule().validate(text: "true@slash-domain.co.uk"))
        XCTAssertTrue(EmailRule().validate(text: "true.withdot@slash-domain.co.uk"))
        XCTAssertTrue(EmailRule().validate(text: "true_underscore@slash-domain.co.uk"))
        XCTAssertTrue(EmailRule().validate(text: "true-middlescore@slash-domain.co.uk"))
        XCTAssertTrue(EmailRule(shouldTrim: true).validate(text: " true@email.com   "))
    }
    
    func testEmailValidationIncorrect() throws {
        XCTAssertFalse(EmailRule().validate(text: "false@.com"))
        XCTAssertFalse(EmailRule().validate(text: "false@"))
        XCTAssertFalse(EmailRule().validate(text: "almostcorrect@email..com"))
        XCTAssertFalse(EmailRule().validate(text: "false@twodomains@twodomains.com"))
        XCTAssertFalse(EmailRule().validate(text: "false@twodomains.com@twodomains.com"))
        XCTAssertFalse(EmailRule().validate(text: " false@email.com"))
        XCTAssertFalse(EmailRule().validate(text: "false@email.com "))
    }
    
    func testAllLowercase() throws {
        XCTAssertTrue(AllLowerCaseRule().validate(text: "alllowercased"))
        XCTAssertTrue(AllLowerCaseRule().validate(text: "all lower cased"))
        
        XCTAssertFalse(AllLowerCaseRule().validate(text: "Notalllowercased"))
        XCTAssertFalse(AllLowerCaseRule().validate(text: "NotallloWercased"))
        XCTAssertFalse(AllLowerCaseRule().validate(text: "NotallloWercaseD"))
        XCTAssertFalse(AllLowerCaseRule().validate(text: "notallloWercaseD"))
        XCTAssertFalse(AllLowerCaseRule().validate(text: "notallloWercaseD"))
        XCTAssertFalse(AllLowerCaseRule().validate(text: "notalllowercaseD"))
        XCTAssertFalse(AllLowerCaseRule().validate(text: "notalllowercaseD"))
        XCTAssertFalse(AllLowerCaseRule().validate(text: "not All lower cased"))
        XCTAssertFalse(AllLowerCaseRule().validate(text: "not all loWer cased"))
    }
    
    func testAllUppercase() throws {
        XCTAssertTrue(AllUpperCaseRule().validate(text: "ALLUPPERCASED"))
        XCTAssertTrue(AllUpperCaseRule().validate(text: "ALL UPPER CASED"))
        
        XCTAssertFalse(AllUpperCaseRule().validate(text: "nOTALLUPPERCASED"))
        XCTAssertFalse(AllUpperCaseRule().validate(text: "nOTALLuPPERCASED"))
        XCTAssertFalse(AllUpperCaseRule().validate(text: "nOTALLuPPERcASED"))
        XCTAssertFalse(AllUpperCaseRule().validate(text: "NOTALLuPPERcASED"))
        XCTAssertFalse(AllUpperCaseRule().validate(text: "NOTALLUPPERcASED"))
        XCTAssertFalse(AllUpperCaseRule().validate(text: "NOTALLuPPERCASED"))

        XCTAssertFalse(AllUpperCaseRule().validate(text: "NOT aLL UPPER CASED"))
        XCTAssertFalse(AllUpperCaseRule().validate(text: "NOT ALL UpPER CASED"))
    }
    
    func testContains() throws {
        XCTAssertTrue(ContainsRule(target: "asd").validate(text: "asd"))
        XCTAssertTrue(ContainsRule(target: "asd").validate(text: "arroundasdaround"))
        XCTAssertTrue(ContainsRule(target: "asd").validate(text: "arround asd around"))
        XCTAssertTrue(ContainsRule(target: "asd").validate(text: "arround asd"))
        XCTAssertTrue(ContainsRule(target: "asd").validate(text: "arroundasd"))
        XCTAssertTrue(ContainsRule(target: "asd").validate(text: "asd around"))
        XCTAssertTrue(ContainsRule(target: "asd").validate(text: "asdaround"))
        XCTAssertTrue(ContainsRule(target: "Asd", ignoreCase: true).validate(text: "asd"))
        XCTAssertTrue(ContainsRule(target: "asd", ignoreCase: true).validate(text: "Asd"))
        XCTAssertTrue(ContainsRule(target: "asd", ignoreCase: true).validate(text: "ASd"))
        XCTAssertTrue(ContainsRule(target: "ASd", ignoreCase: true).validate(text: "asd"))
        
        XCTAssertFalse(ContainsRule(target: "asd").validate(text: ""))
        XCTAssertFalse(ContainsRule(target: "").validate(text: "asd"))
        XCTAssertFalse(ContainsRule(target: "Asd").validate(text: "asd"))
        XCTAssertFalse(ContainsRule(target: "asd").validate(text: "Asd"))
        XCTAssertFalse(ContainsRule(target: "asd").validate(text: "xyz"))
    }
    
    func testMinLength() throws {
        XCTAssertTrue(MinLengthRule(minLength: 5).validate(text: "123456"))
        XCTAssertTrue(MinLengthRule(minLength: 0).validate(text: "1"))
        XCTAssertTrue(MinLengthRule(minLength: 0).validate(text: ""))
        XCTAssertTrue(MinLengthRule(minLength: 1).validate(text: "1"))
        
        XCTAssertFalse(MinLengthRule(minLength: 1).validate(text: ""))
        XCTAssertFalse(MinLengthRule(minLength: 5).validate(text: "123"))
    }
    
    func testMaxLength() throws {
        XCTAssertTrue(MaxLengthRule(maxLength: 5).validate(text: "1234"))
        XCTAssertTrue(MaxLengthRule(maxLength: 0).validate(text: ""))
        XCTAssertTrue(MaxLengthRule(maxLength: 1).validate(text: ""))
        XCTAssertTrue(MaxLengthRule(maxLength: 1).validate(text: "1"))
        
        XCTAssertFalse(MaxLengthRule(maxLength: 0).validate(text: "1"))
        XCTAssertFalse(MaxLengthRule(maxLength: 5).validate(text: "123456"))
    }
    
    func testIsNumber() throws {
        XCTAssertTrue(IsNumberRule().validate(text: "115"))
        XCTAssertTrue(IsNumberRule().validate(text: "-10"))
        XCTAssertTrue(IsNumberRule().validate(text: "1000000"))
        XCTAssertTrue(IsNumberRule().validate(text: "115.54"))
        XCTAssertTrue(IsNumberRule().validate(text: "115.05"))
        XCTAssertTrue(IsNumberRule().validate(text: "-10.05"))
        XCTAssertTrue(IsNumberRule().validate(text: "-10.00000"))
        XCTAssertTrue(IsNumberRule().validate(text: "5e10"))
        
        XCTAssertFalse(IsNumberRule().validate(text: "text"))
        XCTAssertFalse(IsNumberRule().validate(text: "123a123"))
    }
    
    func testGreaterThan() throws {
        XCTAssertTrue(GreaterThanRule(target: 0).validate(text: "1"))
        XCTAssertTrue(GreaterThanRule(target: 5).validate(text: "6"))
        XCTAssertTrue(GreaterThanRule(target: 5).validate(text: "123"))
        XCTAssertTrue(GreaterThanRule(target: -1).validate(text: "0"))
        XCTAssertTrue(GreaterThanRule(target: 5.4).validate(text: "5.5"))
        XCTAssertTrue(GreaterThanRule(target: 5.499999999).validate(text: "5.5"))
        XCTAssertTrue(GreaterThanRule(target: 5.999999999).validate(text: "6"))
        
        XCTAssertFalse(GreaterThanRule(target: 0).validate(text: ""))
        XCTAssertFalse(GreaterThanRule(target: 0).validate(text: "NaN"))
        XCTAssertFalse(GreaterThanRule(target: 0).validate(text: "0"))
        XCTAssertFalse(GreaterThanRule(target: 5).validate(text: "5"))
        XCTAssertFalse(GreaterThanRule(target: 5).validate(text: "0"))
        XCTAssertFalse(GreaterThanRule(target: 5.5).validate(text: "5.4"))
        XCTAssertFalse(GreaterThanRule(target: 5.5).validate(text: "5.499999999"))
        XCTAssertFalse(GreaterThanRule(target: 6).validate(text: "5.999999999"))
    }
    
    func testLessThan() throws {
        XCTAssertTrue(LessThanRule(target: 5).validate(text: "4"))
        XCTAssertTrue(LessThanRule(target: 5).validate(text: "0"))
        XCTAssertTrue(LessThanRule(target: 123).validate(text: "5"))
        XCTAssertTrue(LessThanRule(target: 5.5).validate(text: "5.4"))
        XCTAssertTrue(LessThanRule(target: 5.5).validate(text: "5.499999999"))
        XCTAssertTrue(LessThanRule(target: 6).validate(text: "5.999999999"))
        
        XCTAssertFalse(LessThanRule(target: 0).validate(text: ""))
        XCTAssertFalse(LessThanRule(target: 0).validate(text: "NaN"))
        XCTAssertFalse(LessThanRule(target: 0).validate(text: "0"))
        XCTAssertFalse(LessThanRule(target: 0).validate(text: "1"))
        XCTAssertFalse(LessThanRule(target: 5).validate(text: "6"))
        XCTAssertFalse(LessThanRule(target: 5).validate(text: "123"))
        XCTAssertFalse(LessThanRule(target: -1).validate(text: "0"))
        XCTAssertFalse(LessThanRule(target: 5.4).validate(text: "5.5"))
        XCTAssertFalse(LessThanRule(target: 5.499999999).validate(text: "5.5"))
        XCTAssertFalse(LessThanRule(target: 5.999999999).validate(text: "6"))
    }
    
    func testNoWhitespace() throws {
        XCTAssertTrue(NoWhitespaceRule().validate(text: ""))
        XCTAssertTrue(NoWhitespaceRule().validate(text: "nowhitespace"))
        XCTAssertTrue(NoWhitespaceRule().validate(text: "no_whitespace"))
        
        XCTAssertFalse(NoWhitespaceRule().validate(text: " "))
        XCTAssertFalse(NoWhitespaceRule().validate(text: "    "))
        XCTAssertFalse(NoWhitespaceRule().validate(text: " whitespace"))
        XCTAssertFalse(NoWhitespaceRule().validate(text: "whitespace "))
        XCTAssertFalse(NoWhitespaceRule().validate(text: " whitespace "))
        XCTAssertFalse(NoWhitespaceRule().validate(text: "white space"))
    }

//    func testPerformanceExample() throws {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }

}
