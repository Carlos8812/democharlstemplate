//
//  ViewController.swift
//  DemoCharls
//
//  Created by Charls Salazar on 20/10/21.
//

import UIKit
import TemplateSDK


extension UINavigationController {
    func printMemoryAddress() {
        print(Unmanaged.passUnretained(self).toOpaque())
    }
}

class AppStyles: TemplateStylesDelegate {
    var pageControlStyle: TemplatePageControlStyle?
    
    var buttonStyle: TemplateButtonStyle? = nil
    
    var buttonBorderLessStyle: TemplateButtonStyle?
    
    var backgroundColor: UIColor? {
        return getColorFromAssets("backgroundColor")
    }
    
    var textOnBackgroundColor: UIColor? {
        return getColorFromAssets("textOnBackgroundColor")
    }
    
    var editTextFont: UIFont? = UIFont.systemFont(ofSize: 15)
    
    var navBarColor: UIColor? {
        return getColorFromAssets("navBarColor")
    }
    var navBarItemsColor: UIColor? {
        return getColorFromAssets("navBarItemsColor")
    }
    
    var navBarTextFont: UIFont? {
        return UIFont.systemFont(ofSize: 22, weight: .bold)
    }
    
    
    var labelTitleStyle: TemplateLabelStyle? {
        return TemplateLabelStyle(
            textColor: getColorFromAssets("textOnBackgroundColor") ?? UIColor.darkText, textFont: .boldSystemFont(ofSize: 22)
        )
    }
    
    var labelLargeStyle: TemplateLabelStyle? {
        return TemplateLabelStyle(
            textColor: getColorFromAssets("textOnBackgroundColor") ?? UIColor.darkText, textFont: .systemFont(ofSize: 22)
        )
    }
    
    var labelMediumStyle: TemplateLabelStyle? {
        return TemplateLabelStyle(
            textColor: getColorFromAssets("textOnBackgroundColor") ?? UIColor.darkText, textFont: .systemFont(ofSize: 17)
        )
    }
    
    var labelSmallStyle: TemplateLabelStyle? {
        return TemplateLabelStyle(
            textColor: getColorFromAssets("textOnBackgroundColor") ?? UIColor.darkText, textFont: .systemFont(ofSize: 13)
        )
    }
    
    var labelFieldNameStyle: TemplateLabelStyle? {
        return TemplateLabelStyle(
            textColor: getColorFromAssets("textOnBackgroundColor") ?? UIColor.darkText, textFont: .boldSystemFont(ofSize: 17)
        )
    }
    
}

class ViewController: UIViewController {
    private var showThisVC = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        embededdTemplatee()
    }

    func embededdTemplatee(){
        TemplateInstance.initialize(
            styles: AppStyles(),
            apiUrl: "http://3.13.255.149",
            loginUrl: "/api/login",
            signUpUrl: "/api/register",
            recoveryUrl: "/api/forgetpassword",
            getProfileUrl: "/api/profile",
            updateProfileUrl: "/api/profile/update",
            updateDeviceTokenUrl: "/api/add_device",
            instanceLoginVC: { () -> AuthLoginViewController in
                let vc = AuthLogin.loadFromNib { (success, resObj) -> UIViewController? in
                    if let user = resObj as? TemplateUser {
                        TemplateNetworkUtils.defaultHeaders = ["Authorization": "Bearer \(user.token ?? "")"]
                    }
                    return nil
                }
                vc.hideNavBar = true
                vc.isEmailField = true
                return vc
            },
            instanceSignUpVC: { () -> AuthSignUpViewController in
                let vc = AuthSignUp.loadFromNib { (success, resObj) -> UIViewController? in
                    if let user = resObj as? TemplateUser {
                        TemplateNetworkUtils.defaultHeaders = ["Authorization": "Bearer \(user.token ?? "")"]
                    }
                    return nil
                }
                vc.hideNavBar = true
                vc.isEmailField = true
                return vc
            },
            instanceDashboardVC : { () -> TemplateDashboardViewController in
                let vc = TemplateDashboard.loadFromNib(true, .left)
                vc.hideNavBar = false
                return vc
            },
            instanceProfileVC: { () -> TemplateProfileViewController in
                let vc = TemplateProfile.loadFromNib()
                vc.hideNavBar = true
                return vc
            },
            instanceAboutVC: { () -> TemplateAboutViewController in
                let vc = TemplateAbout.loadFromNib(aboutText: "About Message", isHtmlText: false)
                vc.hideNavBar = true
                
                return vc
            }
        )
                
    
        
        let slider = SliderViewController.sharedInstance!
        let sliderMenu = slider.instanceAndSetTemplateSliderMenu()
        sliderMenu.shareAppLink = "https://templatedemo.page.link/app"
        sliderMenu.delegate = self
        
        if !showThisVC {
            if TemplateUserDefaults.sharedInstance.user?.token?.isEmpty == false {
                dashboardClickHandler(nil)
            } else {
                let vc = AuthSelectorViewController.loadFromNib()
                self.navigationController?.setViewControllers([vc], animated: false)
            }
        }
    }
  
    func dashboardClickHandler(_ sender: Any?) {
        let user = TemplateUserDefaults.sharedInstance.user
        TemplateNetworkUtils.defaultHeaders["Authorization"] = "Bearer \(user?.token ?? "")"
        let vc = TemplateDashboard.loadFromNib(true, .left)
        vc.hideNavBar = false
        vc.title = "Template Demo Charls"
        self.navigationController?.setViewControllers([
            vc
        ], animated: true)
    }
}

extension ViewController: TemplateButtonDelegate {
    func clickHandler(_ button: TemplateButton) {
        let vc = AuthSelectorViewController.loadFromNib()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension ViewController: TemplateSliderMenuDelegate {
    func getCustomCellHeight(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat? {
        return nil
    }
    
    func getCustomCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, rows: [Any]) -> UITableViewCell? {
        return nil
    }

    func rowClickHandler(_ row: Any) {
        if let row = row as? TemplateSliderMenu {
            switch row.text {
            case "Logout":
                let vc = AuthSelectorViewController.loadFromNib()
                (AppDelegate.sharedInstance.window?.rootViewController as? UINavigationController)?.setViewControllers([vc], animated: true)
            default:
                break
            }
        }
    }
}

