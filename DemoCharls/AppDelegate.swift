//
//  AppDelegate.swift
//  DemoCharls
//
//  Created by Charls Salazar on 20/10/21.
//

import UIKit
import TemplateSDK


@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    static var sharedInstance: AppDelegate!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        AppDelegate.sharedInstance = self
        if TemplateUserDefaults.getNotificationStatus() != false {
            TemplateUtils.initNotifications(nil) { (res) in
                print("Init Notification Result: \(res)")
                
                (SliderViewController.sharedInstance?.childView as? TemplateSliderMenuViewController)?.table.reloadData()
            }
        }
        
        let rootModule = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController")
        let _ = setUpWindow([rootModule])
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    func setUpWindow(_ viewControllers: [UIViewController]) -> UIWindow? {
        let navigationController = UINavigationController()
        navigationController.viewControllers = viewControllers
        navigationController.setNavigationBarHidden(true, animated: false)
        
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        
        return window
    }

}

